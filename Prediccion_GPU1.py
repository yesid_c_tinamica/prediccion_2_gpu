import numpy as np
import os
import six.moves.urllib as urllib
import sys
import tarfile
import tensorflow as tf
import zipfile

from distutils.version import StrictVersion
from collections import defaultdict
from io import StringIO
from matplotlib import pyplot as plt
from PIL import Image
import cv2
import StartIR as ini
from object_detection.utils import label_map_util
from object_detection.utils import visualization_utils as vis_util


# print(plt.__file__)
# This is needed since the notebook is stored in the object_detection folder.
sys.path.append("..")
from object_detection.utils import ops as utils_ops
from io import StringIO
import argparse

# Nuevas librerias - Tinamica Yesid Caicedo
  
from datetime import datetime
import pandas as pd
import json
import time

tf.debugging.set_log_device_placement(True)

def run_inference_for_single_image(image, graph):

  tf.debugging.set_log_device_placement(True)

  time_graph=time.time()

  with graph.as_default():

    time_graph_fin= time.time()

    print('TIME copia grafo:',time_graph_fin-time_graph)


    with tf.device('/GPU:1'):

    #sess_config = tf.compat.v1.ConfigProto()
    #sess_config.gpu_options.visible_device_list = '0,1'
    #sess_config.gpu_options.allow_growth = True

        with tf.compat.v1.Session() as sess:

          # Get handles to input and output tensors
          ops = tf.compat.v1.get_default_graph().get_operations()
          all_tensor_names = {output.name for op in ops for output in op.outputs}

          tensor_dict = {}
          for key in [
              'num_detections', 'detection_boxes', 'detection_scores',
              'detection_classes'#, 'detection_masks'
          ]:
            tensor_name = key + ':0'
            if tensor_name in all_tensor_names:
              tensor_dict[key] = tf.compat.v1.get_default_graph().get_tensor_by_name(
                  tensor_name)

          image_tensor = tf.compat.v1.get_default_graph().get_tensor_by_name('image_tensor:0')

          # Run inference
          time_run = time.time()

          # Expand dimensions since the model expects images to have shape: [1, None, None, 3]
          #with tf.device('/device:GPU:1'):
          output_dict = sess.run(tensor_dict,
                                 feed_dict={image_tensor: np.expand_dims(image, 0)})

          time_run_fin = time.time()
          run_inf= time_run_fin - time_run

          print('time run inference:',time_run_fin-time_run)

          # all outputs are float32 numpy arrays, so convert types as appropriate
          output_dict['num_detections'] = int(output_dict['num_detections'][0])
          output_dict['detection_classes'] = output_dict[
              'detection_classes'][0].astype(np.uint16)
          output_dict['detection_boxes'] = output_dict['detection_boxes'][0]
          output_dict['detection_scores'] = output_dict['detection_scores'][0]


  return (output_dict,run_inf)


def predecir():
    print('gpus disponibles dentro funcion PREDECIR:', tf.test.gpu_device_name())
    gpus = tf.config.experimental.list_physical_devices('GPU')
    print(gpus)

    # muestra cual dispositivo (cpu o gpu) ejecuta las operaciones.
    #tf.debugging.set_log_device_placement(True)

    # elige automaticamente dispositivo existente y compatible para las operaciones/
    #tf.config.set_soft_device_placement(True)
    #with tf.device('/GPU:1'):
    inf_mean =['a']
    time_media_open= ['a']
    time_image_array = ['a']
    time_lista_nms = ['a']
    time_write_lista = ['a']
    time_list_pred = ['a']
    time_total_predicion = ['a']
    lista_time_json = ['a']

    # declaración listas para guardar coordenadas predecidas, clase y nombre file image.
    day = repr(datetime.today().day)
    mes = repr(datetime.today().month)
    year = repr(datetime.today().year)
    minute= repr(datetime.now().minute)
    hora= repr(datetime.now().hour)
    segundo=repr(datetime.now().second)
    #df_coor_base=pd.DataFrame()
    json_coor_pred={}
    contar_i=0

    for image_path in TEST_IMAGE_PATHS:

        time_pred = time.time()

        image = Image.open(image_path)

        time_open_image = time.time()
        tm_open = time_open_image - time_pred

        time_media_open.append(tm_open)
        print('time open_imagen:', time_open_image -time_pred)


        # the array based representation of the image will be used later in order to prepare the
        # result image with boxes and labels on it.
        time_load_image = time.time()
        image_np = ini.load_image_into_numpy_array(image)   # ini
        time_load_image_fin = time.time()

        t_array_image=time_load_image_fin-time_load_image
        time_image_array.append(t_array_image)
        print('time load image:', time_load_image_fin-time_load_image)

        #with tf.device('/GPU:1'):
            # Actual detection.b
            # ini.detection_graph
        (output_dict, run_inf) = run_inference_for_single_image(image_np, ini.detection_graph)

        inf_mean.append(run_inf)

        ### Non Max Supression

        time_nms = time.time()

        tf_boxes = tf.convert_to_tensor(output_dict['detection_boxes'], name = 'tf_boxes')
        tf_scores = tf.convert_to_tensor(output_dict['detection_scores'], name = 'tf_scores')

        nms_index = tf.image.non_max_suppression(tf_boxes, tf_scores, max_output_size = 2000, iou_threshold = 0.5, name = 'nms_index')

        df_concat_detections = pd.DataFrame()
        for key in ['detection_boxes', 'detection_scores', 'detection_classes']:

            dict_to_df = pd.DataFrame.from_dict(output_dict[ key ])

            df_concat_detections = pd.concat([df_concat_detections, dict_to_df], axis=1)

        df_concat_detections.columns = ['coor0', 'coor1', 'coor2', 'coor3', 'scores', 'clases']

        # extract information with the same format to out_dict
        df_to_dict_boxes = df_concat_detections.iloc[nms_index.numpy().tolist(), 0:4].to_dict('split')['data']
        df_to_dict_scores = df_concat_detections.iloc[nms_index.numpy().tolist(), 4:5].to_dict('list')
        df_to_dict_clases = df_concat_detections.iloc[nms_index.numpy().tolist(), 5:6].to_dict('list')

        # Replace values output_dic with boulding box selected
        output_dict['num_detections'] = np.int(len(df_to_dict_clases['clases']))
        output_dict['detection_boxes'] = np.array(df_to_dict_boxes, dtype='float32')
        output_dict['detection_scores'] = np.array(df_to_dict_scores['scores'], dtype='float32')
        output_dict['detection_classes'] = np.array(df_to_dict_clases['clases'], dtype='uint16')

        time_nms_fin = time.time()

        t_mns = time_nms_fin-time_nms

        time_lista_nms.append(time_nms_fin-time_nms)
        print('time_nms:',time_nms_fin-time_nms)

        time_visualizar=time.time()
        # Visualization of the results of a detection.
        imageBo,json_coor_pred_individual = vis_util.visualize_boxes_and_labels_on_image_array(
            image_np,
            output_dict['detection_boxes'],
            output_dict['detection_classes'],
            output_dict['detection_scores'],
            CATEGORIY_INDEX,
            instance_masks=output_dict.get('detection_masks'),
            use_normalized_coordinates=True,
            line_thickness=3, skip_scores = False, max_boxes_to_draw = None)

        time_visualizar_fin=time.time()

        time_vis=time_visualizar_fin - time_visualizar
        print('time graficar boxes en image_array:',time_vis)


        time_write_img=time.time()

        ruta = os.path.join(args.outdir, os.path.basename(image_path))
        cv2.imwrite(ruta, cv2.cvtColor(imageBo.astype(np.uint8), cv2.COLOR_RGB2BGR))

        time_write_img_fin = time.time()

        t_write= time_write_img_fin-time_write_img

        time_write_lista.append(t_write)
        print('time guardar foto con inferencias:', time_write_img_fin-time_write_img)

        contar_i = contar_i + 1

        print('imagen:', os.path.basename(image_path), 'imagen:', contar_i, 'de',len(TEST_IMAGE_PATHS))

        ## Función para obtener las coordenadas predecidas, clase y nombre imagen -Yesid Tinamica
        json_coor_pred[os.path.basename(image_path)]={'resumen_predicciones':json_coor_pred_individual}

        time_pred_fin = time.time()

        t_pred = time_pred_fin - time_pred

        time_list_pred.append(t_pred)
        print('time_pred:', time_pred_fin - time_pred)

    time_json = time.time()

    with open(args.outdir+'/json_coor_predecidas_'+day+'_'+mes+'_'+year+'_'+hora+'_'+minute+'_'+segundo+'.json','w') as file_json:
        json.dump(json_coor_pred,file_json,indent=2)

    time_json_fin = time.time()

    t_json = time_json_fin-time_json

    lista_time_json.append(t_json)
    print('time_ json:', time_json_fin-time_json)
    print('time TOTAL predicciòn:',time_json_fin - time_model,'total images:', contar_i)
    t_total = time_json_fin - time_model
    time_total_predicion.append(t_total)

    '''
    print('promedio time open imagen:', sum(time_media_open[1:]) / contar_i)
    print('promedio time image arrayn:', sum(time_image_array[1:]) / contar_i)
    print('promedio time nms :', sum(time_lista_nms[1:]) / contar_i)
    print('promedio time write image :', sum(time_write_lista[1:]) / contar_i)
    print('promedio time run inference:', sum(inf_mean[1:])/contar_i)
    print('promedio time prediccion por imagen:', sum(time_list_pred[1:]) / contar_i)
    '''
    print('gpus disponibles ULTIMA LINEA del COdigo:',tf.test.gpu_device_name())
    gpus=tf.config.experimental.list_physical_devices('GPU')
    print(gpus)

PATH_TO_TEST_IMAGES_DIR = 'images/test/'
TEST_IMAGE_PATHS = []
CATEGORIY_INDEX = {}

def cargarRutaImagenes(path):    
    if path == None:
        path = PATH_TO_TEST_IMAGES_DIR
    for root, subdirs, files in os.walk(path):
        for file in files:
            if os.path.splitext(file)[1].lower() in ('.jpg', '.jpeg'):
                TEST_IMAGE_PATHS.append(os.path.join(root, file))

    print(TEST_IMAGE_PATHS)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Ruta images para cargar en memoria por defecto images/test")
    parser.add_argument('-d', '--directory', type=str, required=False, help='Directory containing the images', default='images\\val')
    parser.add_argument('--outdir', help='Path to output image directory', default='C:\\ProcesadasYolo')
    parser.add_argument('--label', help='Path to config file.', default='clases\\labelmap.pbtx')
    parser.add_argument('--modelo', help='Path to pb file.', default='modelos\\frozen_inference_graph.pb')
    parser.add_argument('--resize', help='1 para hacer resize 0 para no hacer resize', default='0')
    args = parser.parse_args()
    # if (args.resize == '1'):
    #     rescale_images(args.directory)
    # else:
    #     rescribir_imagen(args.directory)
    '''
    modelo = '/home/tinamica/Documentos/practica_image_recognition/prediccion_new_arquitectura/modelos/frozen_inference_graph.pb'
    label = '/home/tinamica/Documentos/practica_image_recognition/prediccion_new_arquitectura/clases/label_map.pbtxt'
    directory = '/home/tinamica/Documentos/practica_image_recognition/prediccion_new_arquitectura/images/prueba_frozen'
    #directory =='/home/tinamica/Documentos/practica_image_recognition/fotos_test_indicador_faster_rccn_nas/codificacion'
    outdir = '/home/tinamica/Documentos/practica_image_recognition/inferencia_new_arquitecture'
    '''
    tf.debugging.set_log_device_placement(True)

    #with tf.device('/GPU:1'):


    print('gpus disponibles INICIO:', tf.test.gpu_device_name())
    gpus = tf.config.experimental.list_physical_devices('GPU')
    print(gpus)
    (CATEGORIY_INDEX, time_model) = ini.cargarModeloPB(args.modelo, args.label)

    TEST_IMAGE_PATHS = []

    print('gpus disponibles DESPUESes de cargar el modelo:', tf.test.gpu_device_name())
    gpus = tf.config.experimental.list_physical_devices('GPU')
    print(gpus)
    cargarRutaImagenes(args.directory)

    print('gpus disponibles DESPUES DE CARGAR IMAGES y antes de la funcion PREDECIR:', tf.test.gpu_device_name())
    gpus = tf.config.experimental.list_physical_devices('GPU')
    print(gpus)
    predecir()
