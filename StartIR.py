import numpy as np
import os
import six.moves.urllib as urllib
import sys
import tarfile
import tensorflow as tf
import zipfile

from distutils.version import StrictVersion
from collections import defaultdict
from io import StringIO
from matplotlib import pyplot as plt
from PIL import Image

import time

print(plt.__file__)
# This is needed since the notebook is stored in the object_detection folder.
sys.path.append("..")
from object_detection.utils import ops as utils_ops

if StrictVersion(tf.__version__) < StrictVersion('1.12.0'):
  raise ImportError('Please upgrade your TensorFlow installation to v1.12.*.')

from object_detection.utils import label_map_util
from object_detection.utils import visualization_utils as vis_util

# inicializar categories y detectar grafo
CATEGORIY_INDEX = {}

#with tf.device('/device:GPU:1'):
detection_graph = tf.Graph()

# import the TF graph
def cargarModeloPB(nombre, fileClases):

  #with tf.device('/device:GPU:1'):

  time_model = time.time()

  PATH_TO_FROZEN_GRAPH = nombre
  PATH_TO_LABELS = fileClases
  CATEGORIY_INDEX = label_map_util.create_category_index_from_labelmap(PATH_TO_LABELS, use_display_name=True)

  with detection_graph.as_default():
    od_graph_def = tf.compat.v1.GraphDef()
    with tf.io.gfile.GFile(PATH_TO_FROZEN_GRAPH, 'rb') as fid:
      serialized_graph = fid.read()
      od_graph_def.ParseFromString(serialized_graph)

      with tf.device('/GPU:1'):  #tf.device('/device:XLA_GPU:1'):
        tf.import_graph_def(od_graph_def, name='')

    time_model_fin = time.time()

    print('time_carga_model:', time_model_fin - time_model)

  return (CATEGORIY_INDEX, time_model)


def load_image_into_numpy_array(image):
  (im_width, im_height) = image.size
  return np.array(image).reshape((im_height, im_width, 3)).astype(np.uint8) #np.array(image.getdata()).reshape((im_height, im_width, 3)).astype(np.uint8)


