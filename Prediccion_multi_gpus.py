import numpy as np
import os
import six.moves.urllib as urllib
import sys
import tarfile
import tensorflow as tf
import zipfile

from distutils.version import StrictVersion
from collections import defaultdict
from io import StringIO
from matplotlib import pyplot as plt
from PIL import Image
import cv2
#sys.setrecursionlimit(10**6)
import StartIR as ini
from object_detection.utils import label_map_util
from object_detection.utils import visualization_utils as vis_util


# print(plt.__file__)
# This is needed since the notebook is stored in the object_detection folder.
sys.path.append("..")
from object_detection.utils import ops as utils_ops
from io import StringIO
import argparse

# Nuevas librerias - Tinamica Yesid Caicedo
from datetime import date   
from datetime import datetime
import pandas as pd
import time
import json


def par_run_inference_for_single_image(image_par, graph):

  with tf.device('/GPU:0'):

      with graph.as_default():
      #with detection_graph.as_default():

        with tf.compat.v1.Session() as sess1:
          # Get handles to input and output tensors
          ops1 = tf.compat.v1.get_default_graph().get_operations()
          all_tensor_names = {output.name for op in ops1 for output in op.outputs}
          tensor_dict = {}
          for key in [
              'num_detections', 'detection_boxes', 'detection_scores',
              'detection_classes'#, 'detection_masks'
          ]:
            tensor_name = key + ':0'
            if tensor_name in all_tensor_names:
              tensor_dict[key] = tf.compat.v1.get_default_graph().get_tensor_by_name(
                  tensor_name)
          image_tensor = tf.compat.v1.get_default_graph().get_tensor_by_name('image_tensor:0')

          # Run inference
          output_dict_par = sess1.run(tensor_dict, feed_dict={image_tensor: np.expand_dims(image_par, 0)})

          # all outputs are float32 numpy arrays, so convert types as appropriate
          output_dict_par['num_detections'] = int(output_dict_par['num_detections'][0])
          output_dict_par['detection_classes'] = output_dict_par[
              'detection_classes'][0].astype(np.uint8)
          output_dict_par['detection_boxes'] = output_dict_par['detection_boxes'][0]
          output_dict_par['detection_scores'] = output_dict_par['detection_scores'][0]

  return output_dict_par


def impar_run_inference_for_single_image(image_impar, graph):

    with tf.device('/GPU:0'):

        with graph.as_default():
        #with detection_graph.as_default():

            with tf.compat.v1.Session() as sess:
                # Get handles to input and output tensors
                ops1 = tf.compat.v1.get_default_graph().get_operations()
                all_tensor_names = {output.name for op in ops1 for output in op.outputs}
                tensor_dict = {}
                for key in [
                    'num_detections', 'detection_boxes', 'detection_scores',
                    'detection_classes'  # , 'detection_masks'
                ]:
                    tensor_name = key + ':0'
                    if tensor_name in all_tensor_names:
                        tensor_dict[key] = tf.compat.v1.get_default_graph().get_tensor_by_name(
                            tensor_name)
                image_tensor = tf.compat.v1.get_default_graph().get_tensor_by_name('image_tensor:0')

                # Run inference
                output_dict_impar = sess.run(tensor_dict, feed_dict={image_tensor: np.expand_dims(image_impar, 0)})

                # all outputs are float32 numpy arrays, so convert types as appropriate
                output_dict_impar['num_detections'] = int(output_dict_impar['num_detections'][0])
                output_dict_impar['detection_classes'] = output_dict_impar[
                    'detection_classes'][0].astype(np.uint8)
                output_dict_impar['detection_boxes'] = output_dict_impar['detection_boxes'][0]
                output_dict_impar['detection_scores'] = output_dict_impar['detection_scores'][0]

    return output_dict_impar



def predecir():    
    # declaración listas para guardar coordenadas predecidas, clase y nombre file image.
    inici=time.time()

    day = repr(date.today().day)
    mes = repr(date.today().month)
    year = repr(date.today().year)
    minute= repr(datetime.now().minute)
    hora= repr(datetime.now().hour)
    segundo=repr(datetime.now().second)
    #df_coor_base=pd.DataFrame()
    json_coor_pred={}
    
    #for image_path in TEST_IMAGE_PATHS:
    i_img = 0
    while i_img < (len(TEST_IMAGE_PATHS) - 1):

        time_load_img=time.time()
        #image = Image.open(image_path)
        image_par = Image.open(TEST_IMAGE_PATHS[i_img])
        image_impar = Image.open(TEST_IMAGE_PATHS[i_img+1])
        # the array based representation of the image will be used later in order to prepare the
        # result image with boxes and labels on it.
        image_np_par = ini.load_image_into_numpy_array(image_par)
        image_np_impar = ini.load_image_into_numpy_array(image_impar)

        time_load_img_fin = time.time()
        print('time_load_img:', time_load_img_fin-time_load_img)

        # Actual detection.  # image_np
        run1=time.time()
        output_dict_par = par_run_inference_for_single_image(image_np_par, ini.detection_graph) ; output_dict_impar = impar_run_inference_for_single_image(image_np_impar, ini.detection_graph)
        run1_fin = time.time()
        print('Time Run :', run1_fin-run1)

        ### Non Max Supression
        time_nms=time.time()
        tf_boxes_par = tf.convert_to_tensor(output_dict_par['detection_boxes'], name = 'tf_boxes')
        tf_scores_par = tf.convert_to_tensor(output_dict_par['detection_scores'], name = 'tf_scores')

        nms_index_par = tf.image.non_max_suppression(tf_boxes_par, tf_scores_par, max_output_size = 2000, iou_threshold = 0.5, name = 'nms_index_par')

        # Impar
        tf_boxes_impar = tf.convert_to_tensor(output_dict_impar['detection_boxes'], name='tf_boxes')
        tf_scores_impar = tf.convert_to_tensor(output_dict_impar['detection_scores'], name='tf_scores')

        nms_index_impar = tf.image.non_max_suppression(tf_boxes_impar, tf_scores_impar, max_output_size=2000, iou_threshold=0.5, name='nms_index_impar')

        time_nms_fin = time.time()
        print('NON MAX Supression Pasada -time:', time_nms_fin-time_nms)


        time_base_datos=time.time()
        df_concat_detections = pd.DataFrame()
        df_concat_detections_impar = pd.DataFrame()
        for key in ['detection_boxes', 'detection_scores', 'detection_classes']:

            dict_to_df = pd.DataFrame.from_dict(output_dict_par[key])
            df_concat_detections = pd.concat([df_concat_detections, dict_to_df], axis=1)

            #impar
            dict_to_df_impar = pd.DataFrame.from_dict(output_dict_impar[key])
            df_concat_detections_impar = pd.concat([df_concat_detections_impar, dict_to_df_impar], axis=1)

        df_concat_detections.columns = ['coor0', 'coor1', 'coor2', 'coor3', 'scores', 'clases']
        df_concat_detections_impar.columns = ['coor0', 'coor1', 'coor2', 'coor3', 'scores', 'clases']

        # extract information with the same format to out_dict
        df_to_dict_boxes_par = df_concat_detections.iloc[nms_index_par.numpy().tolist(), 0:4].to_dict('split')['data']
        df_to_dict_scores_par = df_concat_detections.iloc[nms_index_par.numpy().tolist(), 4:5].to_dict('list')
        df_to_dict_clases_par = df_concat_detections.iloc[nms_index_par.numpy().tolist(), 5:6].to_dict('list')

        # impar
        df_to_dict_boxes_impar = df_concat_detections_impar.iloc[nms_index_impar.numpy().tolist(), 0:4].to_dict('split')['data']
        df_to_dict_scores_impar = df_concat_detections_impar.iloc[nms_index_impar.numpy().tolist(), 4:5].to_dict('list')
        df_to_dict_clases_impar = df_concat_detections_impar.iloc[nms_index_impar.numpy().tolist(), 5:6].to_dict('list')

        # Replace values output_dic with boulding box selected
        output_dict_par['num_detections'] = np.int(len(df_to_dict_clases_par['clases']))
        output_dict_par['detection_boxes'] = np.array(df_to_dict_boxes_par, dtype='float32')
        output_dict_par['detection_scores'] = np.array(df_to_dict_scores_par['scores'], dtype='float32')
        output_dict_par['detection_classes'] = np.array(df_to_dict_clases_par['clases'], dtype='uint8')

        # impar
        output_dict_impar['num_detections'] = np.int(len(df_to_dict_clases_impar['clases']))
        output_dict_impar['detection_boxes'] = np.array(df_to_dict_boxes_impar, dtype='float32')
        output_dict_impar['detection_scores'] = np.array(df_to_dict_scores_impar['scores'], dtype='float32')
        output_dict_impar['detection_classes'] = np.array(df_to_dict_clases_impar['clases'], dtype='uint8')

        time_base_datos_fin = time.time()
        print('time base de datos:', time_base_datos_fin-time_base_datos)

        # Visualization of the results of a detection.
        time_graficar_boxes_par=time.time()

        imageBo_par, json_coor_pred_individual_par = vis_util.visualize_boxes_and_labels_on_image_array(
            image_np_par,
            output_dict_par['detection_boxes'],
            output_dict_par['detection_classes'],
            output_dict_par['detection_scores'],
            CATEGORIY_INDEX,
            instance_masks=output_dict_par.get('detection_masks'),
            use_normalized_coordinates=True,
            line_thickness=3, skip_scores = False, max_boxes_to_draw = None)

        time_graficar_boxes_par_fin = time.time()

        time_graficar_boxes_impar = time.time()

        imageBo_impar, json_coor_pred_individual_impar = vis_util.visualize_boxes_and_labels_on_image_array(
            image_np_impar,
            output_dict_impar['detection_boxes'],
            output_dict_impar['detection_classes'],
            output_dict_impar['detection_scores'],
            CATEGORIY_INDEX,
            instance_masks=output_dict_impar.get('detection_masks'),
            use_normalized_coordinates=True,
            line_thickness=3, skip_scores = False, max_boxes_to_draw = None)

        time_graficar_boxes_impar_fin = time.time()

        print('time boxes en foto par:',time_graficar_boxes_par_fin-time_graficar_boxes_par)
        print('time boxes en foto par:', time_graficar_boxes_impar_fin - time_graficar_boxes_impar)

        # Ruta
        ruta_par = os.path.join(args.outdir, os.path.basename(TEST_IMAGE_PATHS[i_img]))
        ruta_impar = os.path.join(args.outdir, os.path.basename(TEST_IMAGE_PATHS[i_img+1]))

        time_guardar=time.time()
        # Write
        cv2.imwrite(ruta_par, cv2.cvtColor(imageBo_par.astype(np.uint8), cv2.COLOR_RGB2BGR))
        cv2.imwrite(ruta_impar, cv2.cvtColor(imageBo_impar.astype(np.uint8), cv2.COLOR_RGB2BGR))

        time_guardar_fin = time.time()
        print('time guardar foto:', time_guardar_fin-time_guardar)

        ## Función para obtener las coordenadas predichas, clase y nombre imagen -Yesid Tinamica
        json_coor_pred[os.path.basename(TEST_IMAGE_PATHS[i_img])] = {'resumen_predicciones':json_coor_pred_individual_par}
        json_coor_pred[os.path.basename(TEST_IMAGE_PATHS[i_img+1])] = {'resumen_predicciones': json_coor_pred_individual_impar}

        i_img += 1

    with open(args.outdir+'/json_coor_predecidas_'+day+'_'+mes+'_'+year+'_'+hora+'_'+minute+'_'+segundo+'.json','w') as file_json:
        json.dump(json_coor_pred,file_json,indent=2)

    inici_fin = time.time()
    print('Time total:', inici_fin-inici)

PATH_TO_TEST_IMAGES_DIR = 'images/test/'
TEST_IMAGE_PATHS = []
CATEGORIY_INDEX = {}

def cargarRutaImagenes(path):    
    if path == None:
        path = PATH_TO_TEST_IMAGES_DIR
    for root, subdirs, files in os.walk(path):
        for file in files:
            if os.path.splitext(file)[1].lower() in ('.jpg', '.jpeg'):
                TEST_IMAGE_PATHS.append(os.path.join(root, file))

    print('IMAGES_PATH:',TEST_IMAGE_PATHS)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Ruta images para cargar en memoria por defecto images/test")
    parser.add_argument('-d', '--directory', type=str, required=False, help='Directory containing the images', default='images\\val')
    parser.add_argument('--outdir', help='Path to output image directory', default='C:\\ProcesadasYolo')
    parser.add_argument('--label', help='Path to config file.', default='clases\\labelmap.pbtxt')
    parser.add_argument('--modelo', help='Path to pb file.', default='modelos\\frozen_inference_graph.pb')
    parser.add_argument('--resize', help='1 para hacer resize 0 para no hacer resize', default='0')
    args = parser.parse_args()


    #CATEGORIY_INDEX = cargarModeloPB(args.modelo,args.label)
    '''
    modelo = '/home/tinamica/Documentos/practica_image_recognition/prediccion_new_arquitectura/modelos/frozen_inference_graph.pb'
    label = '/home/tinamica/Documentos/practica_image_recognition/prediccion_new_arquitectura/clases/label_map.pbtxt'
    directory = '/home/tinamica/Documentos/practica_image_recognition/prediccion_new_arquitectura/images/prueba_frozen'
    #directory =='/home/tinamica/Documentos/practica_image_recognition/fotos_test_indicador_faster_rccn_nas/codificacion'
    outdir = '/home/tinamica/Documentos/practica_image_recognition/inferencia_new_arquitecture'
    '''

    #ini.iniciar_gpus()
    #tf.debugging.set_log_device_placement(True)

    #with tf.device('/GPU:1'):

    (CATEGORIY_INDEX, time_model) = ini.cargarModeloPB(args.modelo, args.label)
    # (CATEGORIY_INDEX, time_model) = cargarModeloPB(modelo, label)
    TEST_IMAGE_PATHS = []
    cargarRutaImagenes(args.directory)
    predecir()
