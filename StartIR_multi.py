import numpy as np
import os
import six.moves.urllib as urllib
import sys
import tarfile
import tensorflow as tf
import zipfile

from distutils.version import StrictVersion
from collections import defaultdict
from io import StringIO
from matplotlib import pyplot as plt
from PIL import Image

import time

print(plt.__file__)
# This is needed since the notebook is stored in the object_detection folder.
sys.path.append("..")
from object_detection.utils import ops as utils_ops

if StrictVersion(tf.__version__) < StrictVersion('1.12.0'):
  raise ImportError('Please upgrade your TensorFlow installation to v1.12.*.')

from object_detection.utils import label_map_util
from object_detection.utils import visualization_utils as vis_util

MODEL_NAME = 'ssd_mobilenet_v1_coco_2017_11_17'
MODEL_FILE = MODEL_NAME + '.tar.gz'
DOWNLOAD_BASE = 'http://download.tensorflow.org/models/object_detection/'

# Path to frozen detection graph. This is the actual model that is used for the object detection.
PATH_TO_FROZEN_GRAPH = MODEL_NAME + '/frozen_inference_graph.pb'

# List of the strings that is used to add correct label for each box.
PATH_TO_LABELS = os.path.join('data', 'mscoco_label_map.pbtxt')

def iniciar_gpus():

    tf.debugging.set_log_device_placement(True)
    gpus = tf.config.experimental.list_physical_devices('GPU')

    if gpus:
      # Create 2 virtual GPUs with 1GB memory each
      try:
          tf.config.experimental.set_virtual_device_configuration(
              gpus[0],
              [tf.config.experimental.VirtualDeviceConfiguration(memory_limit=9072),
               tf.config.experimental.VirtualDeviceConfiguration(memory_limit=1072)])
          logical_gpus = tf.config.experimental.list_logical_devices('GPU')
          print(len(gpus), "Physical GPU,", len(logical_gpus), "Logical GPUs")
      except RuntimeError as e:
          # Virtual devices must be set before GPUs have been initialized
          print(e)

    return print('iniciadas gpus virtuales')


MODEL_NAME = 'modelos'
PATH_TO_FROZEN_GRAPH = MODEL_NAME + '/frozen_inference_graph.pb'
PATH_TO_LABELS = 'training/labelmap.pbtxt'
CATEGORIY_INDEX = {}
detection_graph = tf.Graph()

# import the TF graph
def cargarModeloPB(nombre, fileClases):
  #with tf.device('/device:GPU:1'):

  time_model = time.time()

  PATH_TO_FROZEN_GRAPH = nombre
  PATH_TO_LABELS = fileClases
  CATEGORIY_INDEX = label_map_util.create_category_index_from_labelmap(PATH_TO_LABELS, use_display_name=True)
  with detection_graph.as_default():
    od_graph_def = tf.compat.v1.GraphDef()
    with tf.io.gfile.GFile(PATH_TO_FROZEN_GRAPH, 'rb') as fid:
      serialized_graph = fid.read()
      od_graph_def.ParseFromString(serialized_graph)
      tf.import_graph_def(od_graph_def, name='')

  time_model_fin = time.time()

  print('time_carga_model:', time_model_fin - time_model)

  return (CATEGORIY_INDEX, time_model)

def load_image_into_numpy_array(image):
  (im_width, im_height) = image.size
  return np.array(image).reshape((im_height, im_width, 3)).astype(np.uint8) #np.array(image.getdata()).reshape((im_height, im_width, 3)).astype(np.uint8)


