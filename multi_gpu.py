import tensorflow as tf

tf.debugging.set_log_device_placement(True)
gpus = tf.config.experimental.list_physical_devices('GPU')

'''
if gpus:
    # Create 2 virtual GPUs with 1GB memory each
    try:
        tf.config.experimental.set_virtual_device_configuration(
            gpus[0],
            [tf.config.experimental.VirtualDeviceConfiguration(memory_limit=1024),
             tf.config.experimental.VirtualDeviceConfiguration(memory_limit=1024)])
        logical_gpus = tf.config.experimental.list_logical_devices('GPU')
        print(len(gpus), "Physical GPU,", len(logical_gpus), "Logical GPUs")
    except RuntimeError as e:
        # Virtual devices must be set before GPUs have been initialized
        print(e)
'''
strategy = tf.distribute.MirroredStrategy()
with strategy.scope():

    inputs = tf.keras.layers.Input(shape=(1,))
    predictions = tf.keras.layers.Dense(1)(inputs)
    model = tf.keras.models.Model(inputs=inputs, outputs=predictions)
    model.compile(loss='mse',    optimizer=tf.keras.optimizers.SGD(learning_rate=0.2))


    #sess_config = tf.compat.v1.ConfigProto()

    #sess_config.gpu_options.visible_device_list = '0,1'
    #sess_config.gpu_options.allow_growth = True

    #sess_config.gpu_options.per_process_gpu_memory_fraction=0.4

    with tf.compat.v1.Session() as sess:

        devices=sess.list_devices()
        for d in devices:
            print('NAMES dispositivos en la Session:',d.name)


        with tf.device('/device:GPU:0'):

            a = tf.constant([[1.0, 2.0, 3.0], [4.0, 5.0, 6.0]])
            b = tf.constant([[1.0, 2.0], [3.0, 4.0], [5.0, 6.0]])
            c = tf.matmul(a, b)
            print('imprimir gpu 0',c)

        with tf.device('/device:GPU:1'):

            print('listar las dos gpus disponibles:',tf.test.is_gpu_available())
            gpus = tf.config.experimental.list_physical_devices('GPU')
            print(gpus)
            print('is gpu available:',tf.test.is_gpu_available())

            r = tf.constant([[1.0, 2.0, 3.0], [4.0, 5.0, 6.0]])
            z = tf.constant([[1.0, 2.0, 3.0], [4.0, 5.0, 6.0]])
            d = tf.add(r, z)
            print('imprimir gpu 1',d)







